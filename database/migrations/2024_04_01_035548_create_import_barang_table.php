<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('import_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->uuid('Id_simulasi')->nullable(false);
            $table->string('Kode_barang', 8);
            $table->string('Uraian_barang', 200);
            $table->integer('Bm');
            $table->float('Nilai_komoditas');
            $table->float('Nilai_bm');
            $table->timestamp('Waktu_insert')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('import_barang');
    }
};
