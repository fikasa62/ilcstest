<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Simulasi Biaya Impor</title>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #f2f2f2;
    }

    .container {
        max-width: 600px;
        margin: 50px auto;
        background-color: #fff;
        border-radius: 8px;
        box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
        padding: 20px;
    }

    .input-group {
        margin-bottom: 20px;
    }

    .input-group label {
        display: block;
        margin-bottom: 5px;
    }

    .input-group input {
        width: 100%;
        padding: 10px;
        border-radius: 5px;
        border: 1px solid #ccc;
    }

    .input-group input[type="submit"] {
        background-color: #4CAF50;
        color: #fff;
        border: none;
        cursor: pointer;
    }

    .input-group input[type="submit"]:hover {
        background-color: #45a049;
    }
    .alert.error{
        background-color: #b71c27;
        border: 1px solid #b71c27;
    }
    .alert.success{
        background-color: #1e8e33;
        border: 1px solid #1e8e33;
    }
    .alert {
        padding: 10px;

        color: #efefef;
        border-radius: 5px;
        margin-bottom: 10px;
        display: flex;
        align-items: center;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 20px;
    }

    th, td {
        border: 1px solid #ccc;
        padding: 8px;
        text-align: left;
    }

    th {
        background-color: #f2f2f2;
    }

    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    tr:hover {
        background-color: #e9e9e9;
    }
</style>
</head>
<body>

<div class="container">
    <h2>Simulasi Biaya Impor</h2>

    @if (session('success'))
            <div class="alert success">
                <strong>Berhasil: </strong>  {{ session('success') }}.
            </div>
    @endif
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert error">
                <strong>Gagal: </strong> {{$error}}.
            </div>
        @endforeach
    @endif
    <form action="dosearchbarang" method="POST">
        @csrf
        <div class="input-group">
            <label for="inputField">Masukkan Kode Barang:</label>
            <input type="text" id="KodeBarang" name="KodeBarang" value="{{ old('KodeBarang') }}" required>
        </div>
        <div class="input-group">
            <label for="inputField">Nilai Komoditas:</label>
            <input type="text" id="NilaiKomoditas" name="NilaiKomoditas" required value="{{ old('NilaiKomoditas') }}">
        </div>
        <div class="input-group">
            <input type="submit" value="Submit">
        </div>
    </form>
    @if (session('Data'))

    <table>
        <tr>
            <th>ID Simulasi</th>
            <td>{{session('Data')->Id_simulasi}}</td>
        </tr>
        <tr>
            <th>Waktu Insert</th>
            <td>{{\App\Helper\Fikasa::tanggal_indo(session('Data')->Waktu_insert, 0, 0, 1)}}</td>
        </tr>
        <tr>
            <th>Kode Barang</th>
            <td>{{session('Data')->Kode_barang}}</td>
        </tr>
        <tr>
            <th>Uraian Barang</th>
            <td>{{session('Data')->Uraian_barang}}</td>
        </tr>
        <tr>
            <th>BM</th>
            <td>{{session('Data')->Bm}}</td>
        </tr>
        <tr>
            <th>Nilai Komoditas</th>
            <td>{{session('Data')->Nilai_komoditas}}</td>
        </tr>
        <tr>
            <th>Nilai BM</th>
            <td>{{session('Data')->Nilai_bm}}</td>
        </tr>
    </table>
    @endif
</div>



</body>
</html>
