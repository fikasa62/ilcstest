<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportBarang extends Model
{
    use HasFactory;

    protected $table = 'import_barang';

}
