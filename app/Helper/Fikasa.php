<?php

namespace App\Helper;
class Fikasa{

    static $months_full = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );

    static $months_short = array (
        1 =>   'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sep',
        'Okt',
        'Nov',
        'Des'
    );


    static function curlPostJson($url, $params, $post, $headers = array()) {
		if (is_string($post)) {
			$post = $params . $post;
		} else {
			$post = $params + $post;
		}


		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  // penting, karena SSL
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);      // penting, karena SSL
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		if (sizeof($headers)) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		}
		$response = curl_exec($curl);
                if ($response === FALSE) {
                  printf("cUrl error (#%d): %s<br>\n", curl_errno($curl),
                  htmlspecialchars(curl_error($curl)));
                }
                            curl_close($curl);
                            //var_dump($response);
                            return $response;
    }

    static function curlRequestJson($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  // penting, karena SSL
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);      // penting, karena SSL
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
    static function convertNumber($number) {
        return number_format($number, 0);
    }

    static function tanggal_indo($tanggal, $get_only_month = false, $short_month = false, $with_time = false)
	{
		if (!$tanggal) {
			return null;
		}

		$tanggal_waktu = date('Y-m-d H:i:s', strtotime($tanggal));
		$tanggal = date('Y-m-d', strtotime($tanggal));

		$bulan = self::$months_full;
		if ($short_month) {
			$bulan = self::$months_short;
		}

		$split = explode('-', $tanggal);
		if($get_only_month){
			return $bulan[ (int)$split[1] ];
		}

		if ($with_time) {
			$split2 = explode(' ', $tanggal_waktu);
			return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0] . ' ' . $split2[1];
		} else{
			return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
		}
	}


}


?>
