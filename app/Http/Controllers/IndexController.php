<?php

namespace App\Http\Controllers;
use App\Helper\Fikasa;
use App\Models\ImportBarang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class IndexController extends Controller
{
    //

    public function index(){
        return view('index');
    }

    public function dosearchbarang(Request $request){
        $kode = $request->KodeBarang;
        $NilaiKomoditas = $request->NilaiKomoditas;
        if(!$kode){
            return Redirect::back()->withErrors(['msg' => 'Masukkan Kode!']);
        }
        if(!$NilaiKomoditas){
            return Redirect::back()->withErrors(['msg' => 'Masukkan Nilai Komoditas!']);
        }
        $kode = trim($kode);
        $NilaiKomoditas = trim($NilaiKomoditas);
        if(strlen($kode) > 8){
            return Redirect::back()->withInput($request->input())->withErrors(['msg' => 'Panjang Kode tidak boleh lebih dari 8']);
        }

        //API: cari uraian barang
        $url = 'https://insw-dev.ilcs.co.id/my/n/barang?hs_code='.$kode;
        $data = Fikasa::curlRequestJson($url);
        $data_barang = json_decode($data, 1);

        if(!isset($data_barang['code']) || ($data_barang['code'] && $data_barang['code'] != '200')){
            $message = isset($data_barang['message'])?$data_barang['message']:"Data not found!";
            return Redirect::back()->withErrors(['msg' => $message]);
        }

        // API:: cari tarif biaya impor
        $url = 'https://insw-dev.ilcs.co.id/my/n/tarif?hs_code='.$kode;
        $data = Fikasa::curlRequestJson($url);
        $data_tarif = json_decode($data, 1);

        if(!isset($data_tarif['code']) || ($data_tarif['code'] && $data_tarif['code'] != '200')){
            $message = isset($data_tarif['message'])?$data_tarif['message']:"Data Tarif not found!";
            return Redirect::back()->withErrors(['msg' => $message]);
        }

        // dd($data_barang);
        // $Id_simulasi =  0;
        $Kode_barang =  $data_barang['data'][0]['hs_code_format'];
        $Uraian_barang =  $data_barang['data'][0]['uraian_id'];
        $Bm =  $data_tarif['data'][0]['bm'];
        $Nilai_komoditas =  $NilaiKomoditas;
        // nilai komoditas * tarif biaya impor / 100
        $Nilai_bm =  $Nilai_komoditas * $Bm / 100;

        $new = new ImportBarang();
        $new->Id_simulasi = Str::uuid()->toString();;
        $new->Kode_barang = $Kode_barang;
        $new->Uraian_barang = $Uraian_barang;
        $new->Bm = $Bm;
        $new->Nilai_komoditas = $Nilai_komoditas;
        $new->Nilai_bm = $Nilai_bm;
        $new->Waktu_insert = date('Y-m-d H:i:s');
        $new->save();


        return  Redirect::route('index')->with( ['Data' => $new, 'success' => 'Data Simulasi Biaya Impor Berhasil disimpan!'] );;



    }
}
